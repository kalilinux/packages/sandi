#!/usr/bin/env python


import re
import subprocess
from platform import system
import os
import httplib
from random import randint
def shell_storm(value):
    os = system()
    if os == 'Linux':
        config_location = "config/sandi.conf"
    elif os == "Windows":
        config_location = "config\\sandi.conf"
    else:
        config_location = "config/sandi.conf"
    
    conf_file = open(config_location, "r").readlines()
    for cr in conf_file:
        cr = cr.rstrip()  
        match1 = re.search("BROWSER=", cr)
        if match1:
            browser = cr.replace("BROWSER=", "")
            browser = browser+" "
        match2 = re.search("OUTPUT=", cr)
        if match2:
            output = cr.replace("OUTPUT=", "")

    session = randint(11111, 99999)
    filename = "result-shell-storm-%d.html"%session 
    location = output+filename
    head = """
    <!DOCTYPE html>
<html lang="en">
<head>
<title>Sandi Result Page</title>

<style type="text/css">

    html, body, div, span, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    abbr, address, cite, code,
    del, dfn, em, img, ins, kbd, q, samp,
    small, strong, sub, sup, var,
    b, i,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td {
        margin:0;
        padding:0;
        border:0;
        outline:0;
        font-size:100%;
        vertical-align:baseline;
        background:transparent;
    }
    
    body {
        margin:0;
        padding:0;
        font:12px/15px "Helvetica Neue",Arial, Helvetica, sans-serif;
        color: #555;
        background:#f5f5f5 url(bg.jpg);
    }
    
    a {color:#666;}
    
    #content {width:85%; max-width:1000px; margin:1% auto 0;}
    
    /*
    Pretty Table Styling
    CSS Tricks also has a nice writeup: http://css-tricks.com/feature-table-design/
    */
    
    table {
        overflow:hidden;
        border:1px solid #d3d3d3;
        background:#fefefe;
        width:100%;
        margin:5% auto 0;
        -moz-border-radius:5px; /* FF1+ */
        -webkit-border-radius:5px; /* Saf3-4 */
        border-radius:5px;
        -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
        -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
    }
    
    th, td {padding:18px 28px 18px; text-align:center; }
    
    th {padding-top:22px; text-shadow: 1px 1px 1px #fff; background:#e8eaeb;}
    
    td {border-top:1px solid #e0e0e0; border-right:1px solid #e0e0e0;}
    
    tr.odd-row td {background:#f6f6f6;}
    
    td.first, th.first {text-align:left}
    
    td.last {border-right:none;}
    
    /*
    Background gradients are completely unnecessary but a neat effect.
    */
    
    td {
        background: -moz-linear-gradient(100% 25% 90deg, #fefefe, #f9f9f9);
        background: -webkit-gradient(linear, 0% 0%, 0% 25%, from(#f9f9f9), to(#fefefe));
    }
    
    tr.odd-row td {
        background: -moz-linear-gradient(100% 25% 90deg, #f6f6f6, #f1f1f1);
        background: -webkit-gradient(linear, 0% 0%, 0% 25%, from(#f1f1f1), to(#f6f6f6));
    }
    
    th {
        background: -moz-linear-gradient(100% 20% 90deg, #e8eaeb, #ededed);
        background: -webkit-gradient(linear, 0% 0%, 0% 20%, from(#ededed), to(#e8eaeb));
    }
    


</style>

</head>
<body>
<center><h1>Sandi Result From Shell-Storm</h1></center>
<center><img src="http://www.veryicon.com/icon/png/Kids/Petroglyphs/The%20Crab.png" alt="Sandi Logo"></center>
<div id="content">

    <table cellspacing="0">
    <tr><th>Title</th><th>Platform</th><th>Author</th><th>ID</th><th>Link</td></tr>
    """
    
    try:
        file0 = open(location, "w")
        file0.write(head)
        file0.close()
        req = httplib.HTTPConnection("shell-storm.org")
        req.request("GET", "/api/?s="+str(value))
        res = req.getresponse()
        data_1 = res.read().split('\n')
    except Exception, e:
        print "[!]Error : %s"%e
    for data in data_1:
        try:
            data0 = data.split("::::")
            author = data0[0]
            platform = data0[1]
            title = data0[2]
            id = data0[3]
            link = data0[4]
        except(IndexError):
            pass
        if author:
            if platform:
                if title:
                    if id:
                        file1 = open(location, "a")
                        link2 = '<a href="%s">View</a>'%link
                        current_info = "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>"%(title, platform, author, id, link2)
                        file1.write(current_info+"\n")
                        file1.close()
    foot = """
         </table>

</div>

</body>
</html>
"""

    file2 = open(location, "a")
    file2.write(foot)
    file2.close()
    subprocess.Popen(browser+location, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
