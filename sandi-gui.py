#!/usr/bin/env python
# Sandi Exploit & Vulnerability Search Engine
# Author : Fardin Allahverdinazhand (0x0ptim0us)
# Date : 2014/2/1
# Email : 0x0ptim0us@Gmail.Com

import pygtk
pygtk.require("2.0")
import gtk
from lib import exploitdb
from lib import msf
from lib import shell_storm

class MAIN:
    def find(self, widget):
        self.current_keyword = self.entry_search.get_text()
        self.current_database = self.combo_box.get_active_text() 
        if self.current_keyword =="" or self.current_database ==None:
            self.mk = gtk.MessageDialog(parent=None, flags=0, type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_CLOSE, message_format="Check Search Field and Select Database First.")
            self.mk.run()
            self.mk.destroy() 
        if self.current_database =="Exploit-db":
            exploitdb.exploitdb(self.current_keyword)
            
        elif self.current_database =="Metasploit":
            msf.msf(self.current_keyword)
            
        elif self.current_database =="Shell-Storm":
            shell_storm.shell_storm(self.current_keyword)
            
    def destroy(self, widget, data=None):
        gtk.main_quit()
    def about_me(self, widget, data=None):
        myname = ("Fardin Allahverdinazhand", "Nickname : 0x0ptim0us", "Email : 0x0ptim0us@Gmail.Com", "Location : IRAN - Azarbayjan", "Codename : Just like old times")
        license = '''
#            --------------------------------------------------
#               Sandi Exploit Search Engine         
#            --------------------------------------------------
#        Copyright (C) <2014>  <0x0ptim0us (Fardin Allahverdinazhand)>
#
#        This program is free software: you can redistribute it and/or modify
#        it under the terms of the GNU General Public License as published by
#        the Free Software Foundation, either version 3 of the License, or
#        any later version.
#
#        This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#        You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
        about = gtk.AboutDialog()
        about.set_program_name("Sandi")
        about.set_version("1.3")
        about.set_copyright("Copyright (c) <2014> Fardin Allahverdinazhand (0x0ptim0us)")
        about.set_comments("Sandi is a open source tool for search exploits and vulnerbility in the public databases")
        about.set_website("http://sourceforge.net/projects/sandisearch")
        about.set_license(license)
        about.set_authors(myname)
        about.set_logo(gtk.gdk.pixbuf_new_from_file("sandi-icon.png"))
        about.run()
        about.destroy()
    def __init__(self):
        # Main Window
        self.root = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.root.set_title("Sandi V1.3")
        self.root.set_size_request(355, 200)
        self.root.set_resizable(0)
        self.root.set_icon_from_file("sandi-icon.png")
        self.root.set_position(gtk.WIN_POS_CENTER)
        self.root.set_border_width(5)
        self.root.connect("destroy", self.destroy)
        
        #Search Frame
        self.frame1 = gtk.Frame(" Search ")
        self.frame2 = gtk.Frame(" Database ")
        self.frame3 = gtk.Frame(" About & Exit")
        
        #Search Buttons
        self.button_search = gtk.Button("Find", stock=gtk.STOCK_FIND)
        self.button_search.set_size_request(65, 30)
        self.button_search.set_tooltip_text("Find")
        self.button_search.connect("clicked", self.find)
        #Search Entry
        self.entry_search = gtk.Entry()
        self.entry_search.set_tooltip_text("Type Keyword For Search")
        self.entry_search.set_size_request(260, 25)
        
        #Fixed
        fixed = gtk.Fixed()
        fixed.put(self.frame1, 1, 1)
        fixed.put(self.frame2, 1, 60)
        fixed.put(self.frame3, 1, 115)
        #Search boxs
        self.hbox = gtk.HBox(False, 5)
        self.hbox.set_border_width(5)
        self.frame1.add(self.hbox)
        self.hbox.pack_start(self.entry_search)
        self.hbox.pack_start(self.button_search)

        #Combo
        self.combo_box = gtk.combo_box_new_text()
        self.combo_box.set_size_request(330, 28)

        self.combo_box.append_text("Exploit-db")
        self.combo_box.append_text("Metasploit")
        self.combo_box.append_text("Shell-Storm")
               
        #Database Boxs
        self.hbox2 = gtk.HBox(False, 5)
        self.hbox2.set_border_width(5)
        self.frame2.add(self.hbox2)
        self.hbox2.pack_start(self.combo_box)
        
        # About & Exit Button
        self.button_exit = gtk.Button(" Quit ", stock=gtk.STOCK_QUIT)
        self.button_exit.set_size_request(65, 30)
        self.button_exit.set_tooltip_text("Quit from Sandi")
        self.button_exit.connect("clicked", self.destroy)
        self.button_about = gtk.Button(" About ", stock=gtk.STOCK_ABOUT)
        self.button_about.set_tooltip_text("Show About Dialog")
        self.button_about.connect("clicked", self.about_me)
        # About
        self.hbox3 = gtk.HBox(False, 183)
        self.hbox3.set_border_width(10)
        self.frame3.add(self.hbox3)
        self.hbox3.pack_start(self.button_exit)
        self.hbox3.pack_start(self.button_about)
        
        self.root.add(fixed)
        self.root.show_all()
        
        
        
        
        
def main():
    gtk.main()


if __name__ == "__main__":
    root = MAIN()
    main()