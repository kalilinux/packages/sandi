#!/usr/bin/env python
# Sandi Exploit & Vulnerability Search Engine
# Author : Fardin Allahverdinazhand (0x0ptim0us)
# Date : 2014/2/1
# Email : 0x0ptim0us@Gmail.Com

import optparse
from lib import msf
from lib import exploitdb
from lib import shell_storm

def main():
    try:
        my_help = '''
        sandi.py [options]
        Supported Database in this version:
        [+]exploitdb
        [+]msf
        [+]shell-storm
    
        For Example : ./sandy.py -d shell-storm -k bash
        '''
        parse = optparse.OptionParser(usage=my_help)
        parse.add_option('-d', '--database', help='Supported Database Name', default='exploitdb')
        parse.add_option('-k', '--keyword', help="Keyword For Search")
        opt, args=parse.parse_args()
        if opt.database:
            if opt.keyword:
                if opt.database=="exploitdb":
                    exploitdb.exploitdb(opt.keyword)
                elif opt.database=="msf":
                    msf.msf(opt.keyword)
                elif opt.database=="shell-storm":
                    shell_storm.shell_storm(opt.keyword)
                else:
                    print "[!]Error : Database Unsupported !"
                    exit(-1)
            else:
                print "[!]Error : Enter Keyword For Search, Keyword Not Found ! Use --help Switch For Help."
                exit(-1)
        else:
            print "[!]Error : Enter Database Name, Database Not Found !"
            exit(-1)
    except (KeyboardInterrupt):
        print "[!]Error : ^C Detected."
if __name__ == '__main__':
    main()